CREATE TABLE `praise`
(
    `id`          int(11)   NOT NULL AUTO_INCREMENT COMMENT '主键',
    `blog_id`     int(11)   NOT NULL COMMENT '博客id',
    `user_id`     int(11)   NOT NULL COMMENT '点赞人',
    `praise_time` datetime       DEFAULT NULL COMMENT '点赞时间',
    `status`      int(11)        DEFAULT '1' COMMENT '状态(1=正常, 0=取消点赞)',
    `is_active`   int(11)        DEFAULT '1' COMMENT '是否有效(1=是, 0=否)',
    `create_time` datetime       DEFAULT NULL COMMENT '创建时间',
    `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4 COMMENT = '用户点赞记录表';