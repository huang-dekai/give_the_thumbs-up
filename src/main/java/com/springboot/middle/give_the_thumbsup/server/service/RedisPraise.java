package com.springboot.middle.give_the_thumbsup.server.service;

import com.springboot.middle.give_the_thumbsup.server.dto.PraiseRankDto;

import java.util.List;

/**
 * 博客点赞Redis处理服务
 */
public interface RedisPraise {
    /**
     * 缓存当前用户点赞博客的记录-包括正常点赞, 取消点赞
     *
     * @param blogId 博客id
     * @param userId 用户id
     * @param status 状态(1=正常, 0=取消点赞)
     * @throws Exception exception
     */
    void cachePraiseBlog(Integer blogId, Integer userId, Integer status) throws Exception;

    /**
     * 获取当前博客总的点赞数
     *
     * @param blogId 博客id
     * @return 当前博客总的点赞数
     * @throws Exception exception
     */
    Long getCacheTotalBlog(Integer blogId) throws Exception;

    /**
     * 触发博客点赞总数排行榜
     *
     * @throws Exception exception
     */
    void rankBlogPraise() throws Exception;

    /**
     * 获得博客点赞排行榜
     *
     * @return 博客点赞排行榜
     * @throws Exception exception
     */
    List<PraiseRankDto> getBlogPraiseRank() throws Exception;
}
