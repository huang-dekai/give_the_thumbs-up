package com.springboot.middle.give_the_thumbsup.server.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Integer blogId 博客id<br/>
 * Integer userId 用户id
 */
public class PraiseDto implements Serializable {
    @NotNull
    private Integer blogId;
    @NotNull
    private Integer userId;

    public PraiseDto() {
    }

    public PraiseDto(Integer blogId, Integer userId) {
        this.blogId = blogId;
        this.userId = userId;
    }

    public Integer getBlogId() {
        return blogId;
    }

    public void setBlogId(Integer blogId) {
        this.blogId = blogId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PraiseDto{" +
                "blogId=" + blogId +
                ", userId=" + userId +
                '}';
    }
}
