package com.springboot.middle.give_the_thumbsup.server.controller;

import com.google.common.collect.Maps;
import com.springboot.middle.give_the_thumbsup.api.enums.StatusCode;
import com.springboot.middle.give_the_thumbsup.api.response.BaseResponse;
import com.springboot.middle.give_the_thumbsup.server.dto.PraiseDto;
import com.springboot.middle.give_the_thumbsup.server.service.PraiseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class PraiseController {
    private static final Logger log = LoggerFactory.getLogger(PraiseController.class);
    private static final String prefix = "blog/praise";

    @Autowired
    PraiseService praiseService;

    /**
     * 点赞业务controller - 不带redisson缓存
     * @param dto 博客id, 用户id
     * @param result 自带参数, 用来检查是否发生错误
     * @return baseResponse
     */
    @RequestMapping(value = prefix + "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse addPraise(@RequestBody @Validated PraiseDto dto, BindingResult result) {
        //合法性检查
        if (result.hasErrors()) {
            return new BaseResponse(StatusCode.InvalidParams);
        }
        BaseResponse response = new BaseResponse(StatusCode.Success);
        HashMap<String, Object> resMap = Maps.newHashMap();
        try {
            praiseService.addPraise(dto);

            //获取总数
            Long blogPraiseTotal = praiseService.getBlogPraiseTotal(dto.getBlogId());
            resMap.put("praiseTotal",  blogPraiseTotal);
        } catch (Exception e) {
            log.error("点赞博客-发生异常:{}", dto, e.fillInStackTrace());
            response = new BaseResponse(StatusCode.Fail.getCode(), e.getMessage());
        }
        response.setData(resMap);
        return response;
    }

    /**
     * 点赞业务controller - 带redisson缓存
     * @param dto 博客id, 用户id
     * @param result 自带参数, 用来检查是否发生错误
     * @return baseResponse
     */
    @RequestMapping(value = prefix + "/addWithRedisson", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse addPraiseWithRedisson(@RequestBody @Validated PraiseDto dto, BindingResult result) {
        //合法性检查
        if (result.hasErrors()) {
            return new BaseResponse(StatusCode.InvalidParams);
        }
        BaseResponse response = new BaseResponse(StatusCode.Success);
        HashMap<String, Object> resMap = Maps.newHashMap();
        try {
            praiseService.addPraiseLock(dto);

            //获取总点赞数
            Long blogPraiseTotal = praiseService.getBlogPraiseTotal(dto.getBlogId());
            resMap.put("praiseTotal",  blogPraiseTotal);
        } catch (Exception e) {
            log.error("点赞博客-发生异常:{}", dto, e.fillInStackTrace());
            response = new BaseResponse(StatusCode.Fail.getCode(), e.getMessage());
        }
        response.setData(resMap);
        return response;
    }

    /**
     * 取消点赞业务controller - 带redisson缓存
     * @param dto 博客id, 用户id
     * @param result 自带参数, 用来检查是否发生错误
     * @return baseResponse
     */
    @RequestMapping(value = prefix + "/cancel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse cancelPraise(@RequestBody @Validated PraiseDto dto, BindingResult result) {
        //合法性检查
        if (result.hasErrors()) {
            return new BaseResponse(StatusCode.InvalidParams);
        }
        BaseResponse response = new BaseResponse(StatusCode.Success);
        HashMap<String, Object> resMap = Maps.newHashMap();
        try {
            //调用取消点赞业务
            praiseService.cancelPraise(dto);

            //获取总点赞数
            Long blogPraiseTotal = praiseService.getBlogPraiseTotal(dto.getBlogId());
            resMap.put("praiseTotal", blogPraiseTotal);
        } catch (Exception e) {
            log.error("点赞博客-发生异常:{}", dto, e.fillInStackTrace());
            response = new BaseResponse(StatusCode.Fail.getCode(), e.getMessage());
        }
        response.setData(resMap);
        return response;
    }

    /**
     * 获取博客点赞排行榜
     * @return 博客点赞排行榜
     */
    @RequestMapping(value = prefix + "/total/rank", method = RequestMethod.GET)
    public BaseResponse rankPraise() {
        BaseResponse response = new BaseResponse(StatusCode.Success);
        try {
            response.setData(praiseService.getRankWithRedisson());
        } catch (Exception e) {
            log.error("获取点赞排行榜-发生异常", e.fillInStackTrace());
            response = new BaseResponse(StatusCode.Fail.getCode(), e.getMessage());
        }
        return response;
    }
}
