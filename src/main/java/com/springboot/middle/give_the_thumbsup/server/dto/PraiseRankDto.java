package com.springboot.middle.give_the_thumbsup.server.dto;

import java.io.Serializable;

/**
 * Integer blogId 博客id<br/>
 * Long total 点赞总数
 */
public class PraiseRankDto implements Serializable {
    private Integer blogId;
    private Long total;

    public PraiseRankDto() {
    }

    public PraiseRankDto(Integer blogId, Long total) {
        this.blogId = blogId;
        this.total = total;
    }

    public Integer getBlogId() {
        return blogId;
    }

    public void setBlogId(Integer blogId) {
        this.blogId = blogId;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "PraiseRankDto{" +
                "blogId=" + blogId +
                ", total=" + total +
                '}';
    }
}
