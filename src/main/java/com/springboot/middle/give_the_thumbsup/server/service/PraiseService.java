package com.springboot.middle.give_the_thumbsup.server.service;

import com.springboot.middle.give_the_thumbsup.server.dto.PraiseDto;
import com.springboot.middle.give_the_thumbsup.server.dto.PraiseRankDto;

import java.util.Collection;

/**
 * 点赞业务接口
 */
public interface PraiseService {
    /**
     * 点赞博客-无分布式锁
     * @param dto blogId, userId
     * @throws Exception exception
     */
    void addPraise(PraiseDto dto) throws Exception;

    /**
     * 点赞博客-分布式锁
     * @param dto blogId, userId
     * @throws Exception exception
     */
    void addPraiseLock(PraiseDto dto) throws Exception;

    /**
     * 取消点赞博客
     * @param dto blogId, userId
     * @throws Exception exception
     */
    void cancelPraise(PraiseDto dto) throws Exception;

    /**
     * 获取博客的点赞数
     * @param blogId 博客id
     * @return 对应的点赞数
     * @throws Exception exception
     */
    Long getBlogPraiseTotal(Integer blogId) throws Exception;

    /**
     * 获取博客点赞总数排行榜 - 不用缓存
     * @return 排行榜
     * @throws Exception exception
     */
    Collection<PraiseRankDto> getRankNoRedisson() throws Exception;

    /**
     * 获取博客点赞总数排行榜 - 用缓存
     * @return 排行榜
     * @throws Exception exception
     */
    Collection<PraiseRankDto> getRankWithRedisson() throws Exception;
}
