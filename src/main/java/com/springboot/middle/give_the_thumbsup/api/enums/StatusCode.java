package com.springboot.middle.give_the_thumbsup.api.enums;

/**
 * 通用状态码类
 */
public enum StatusCode {
    //暂时设定的几种状态码类
    Success(0, "成功"),
    Fail(-1, "失败"),
    InvalidParams(201, "非法参数!"),
    InvalidGrantType(202, "非法授权类型!");

    private Integer code;   //状态码
    private String msg;     //描述信息

    //重载的构造方法
    StatusCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
