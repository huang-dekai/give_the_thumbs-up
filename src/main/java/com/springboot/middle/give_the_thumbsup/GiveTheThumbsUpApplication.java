package com.springboot.middle.give_the_thumbsup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiveTheThumbsUpApplication {

    public static void main(String[] args) {
        SpringApplication.run(GiveTheThumbsUpApplication.class, args);
    }

}
